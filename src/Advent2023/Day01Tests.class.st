Class {
	#name : #Day01Tests,
	#superclass : #TestCase,
	#instVars : [
		'challenge'
	],
	#category : #Advent2023
}

{ #category : #running }
Day01Tests >> setUp [

	| input |
	super setUp.

	input := '1abc2
pqr3stu8vwx
a1b2c3d4e5f
treb7uchet'.

	challenge := Day01 new withInput: input
]

{ #category : #tests }
Day01Tests >> testFirstPart [
self assert: (challenge firstPart) equals: 142.
]

{ #category : #tests }
Day01Tests >> testSecondPart [

	| input |
	input := 'two1nine
eightwothree
abcone2threexyz
xtwone3four
4nineeightseven2
zoneight234
7pqrstsixteen'.

	challenge := Day01 new withInput: input.
	self assert: (challenge secondPart) equals: 281.

]
