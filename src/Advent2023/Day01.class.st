Class {
	#name : #Day01,
	#superclass : #Object,
	#instVars : [
		'asString',
		'input',
		'd'
	],
	#category : #Advent2023
}

{ #category : #accessing }
Day01 >> firstPart [
	| total firstDigit lastDigit digit|
	total := 0.
	input linesDo: [ :line |
		firstDigit := '(\d)' asRegex.
		lastDigit := '.*(\d)' asRegex.
		firstDigit search: line.
		lastDigit search: line.
		
		digit := (firstDigit subexpression: 2), (lastDigit subexpression: 2).
		total := total + (digit asInteger).
	 ].
	^ total.
]

{ #category : #parsing }
Day01 >> parseLiteralDigit: line withRegex: regex [

	| digit |
	regex search: line.
	digit := regex subexpression: 2.
	digit := d at: digit ifAbsent: [ digit ].
	^ digit
]

{ #category : #'as yet unclassified' }
Day01 >> secondPart [
	| total fragment firstDigit lastDigit digit number |
	fragment := '(', ('|' join: (d keys)), '|\d)'.
	total := 0.
	input linesDo: [ :line |
		firstDigit := fragment asRegex.
		lastDigit := ('.*', fragment) asRegex.
		
		digit := self parseLiteralDigit: line withRegex: firstDigit .
		number := digit, (self parseLiteralDigit: line withRegex: lastDigit).
		
		total := total + (number asInteger).
	].
	^ total.
]

{ #category : #'as yet unclassified' }
Day01 >> withInput: aString [ 
	d := Dictionary newFrom:  { 
		#one -> '1'. 
		#two -> '2'.
		#three -> '3'.
		#four -> '4'.
		#five -> '5'.
		#six -> '6'.
		#seven -> '7'.
		#eight -> '8'.
		#nine -> '9'
	}.
	input := aString.
	^ self.
]
